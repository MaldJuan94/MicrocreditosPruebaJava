package com.juanescorcia.microcreditos.models;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import org.springframework.data.jpa.convert.threeten.Jsr310JpaConverters.LocalDateTimeConverter;

import com.fasterxml.jackson.annotation.JsonIgnore;
//uniqueConstraints = @UniqueConstraint(columnNames = {"documento"})
@Entity
@Table(name="mc_cliente")
public class Cliente implements Serializable{
	@Id
	@Column(name = "pk")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "primer_nombre")
	private String primerNombre;
	
	@Column(name = "segundo_nombre")
	private String segundoNombre;
	
	@Column(name = "primer_apellido")
	private String primerApellido;
	
	@Column(name = "segundo_apellido")
	private String segundoApellido;
	
	@Column(name = "documento")
	private String documento;
	
	@Column(name = "telefono")
	private String telefono;
	
	@Column(name = "direccion")
	private String direccion;
	
	@Transient
	private String NombreCompleto;

	@OneToMany(mappedBy="cliente" ,cascade = CascadeType.ALL)
	@JsonIgnore
	private Set<Credito> creditos;
	
	@Column(name="created_at",insertable = false, updatable = false)
	@Convert(converter = LocalDateTimeConverter.class)
	private LocalDateTime creado;

	@Column(name="update_at" ,insertable = false, updatable = false)
	@Convert(converter = LocalDateTimeConverter.class)
	private LocalDateTime actualizado;


	public Cliente(String primerNombre, String segundoNombre, String primerApellido, String segundoApellido,
			String documento, String telefono, String direccion) {
		super();
		this.primerNombre = primerNombre;
		this.segundoNombre = segundoNombre;
		this.primerApellido = primerApellido;
		this.segundoApellido = segundoApellido;
		this.documento = documento;
		this.telefono = telefono;
		this.direccion = direccion;
		this.NombreCompleto = this.primerNombre + " " + this.primerApellido + ", cc: "+this.documento;
	}

	public void FormatNombreCompleto() {
		this.NombreCompleto = this.primerNombre + " " + this.primerApellido + ", cc: "+this.documento;
	}
	public Cliente() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPrimerNombre() {
		return primerNombre;
	}

	public void setPrimerNombre(String primerNombre) {
		this.primerNombre = primerNombre;
	}

	public String getSegundoNombre() {
		return segundoNombre;
	}

	public void setSegundoNombre(String segundoNombre) {
		this.segundoNombre = segundoNombre;
	}

	public String getPrimerApellido() {
		return primerApellido;
	}

	public void setPrimerApellido(String primerApellido) {
		this.primerApellido = primerApellido;
	}

	public String getSegundoApellido() {
		return segundoApellido;
	}

	public void setSegundoApellido(String segundoApellido) {
		this.segundoApellido = segundoApellido;
	}

	public String getDocumento() {
		return documento;
	}

	public void setDocumento(String documento) {
		this.documento = documento;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public LocalDateTime getCreado() {
		return creado;
	}

	public void setCreado(LocalDateTime creado) {
		this.creado = creado;
	}

	public LocalDateTime getActualizado() {
		return actualizado;
	}

	public void setActualizado(LocalDateTime actualizado) {
		this.actualizado = actualizado;
	}

	public Set<Credito> getCreditos() {
		return creditos;
	}

	public void setCreditos(Set<Credito> creditos) {
		this.creditos = creditos;
	}

	public String getNombreCompleto() {
		return NombreCompleto;
	}

	public void setNombreCompleto(String nombreCompleto) {
		NombreCompleto = nombreCompleto;
	}
	
	
	
	
}
