package com.juanescorcia.microcreditos.models;
import java.io.Serializable;
import java.sql.Date;
import java.time.LocalDateTime;
import java.time.ZoneId;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.data.jpa.convert.threeten.Jsr310JpaConverters.LocalDateTimeConverter;

import com.juanescorcia.microcreditos.util.Utils;
@Entity
@Table(name="mc_credito")
public class Credito implements Serializable{
	@Id
	@Column(name = "pk")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@ManyToOne(optional = true,fetch=FetchType.EAGER)
	@JoinColumn(name="id_cliente")
	private Cliente cliente;	
	
	@Column(name = "valor_prestamo")
	private double valorPrestamo;
	
	@Column(name = "interes")
	private double interes;
	
	@Column(name = "pagado")
	private boolean pagado;
	
	@Transient
	private double total;
	
	@Transient
	private int dias;
	
	@Transient
	private double cobroDia;
	
	@Transient
	private java.util.Date fechaFinCredito;
	
	@Column(name = "fecha_credito")
	@Convert(converter = LocalDateTimeConverter.class)
	private LocalDateTime fechaCredito;
	
	@Column(name="created_at",insertable = false, updatable = false)
	@Convert(converter = LocalDateTimeConverter.class)
	private LocalDateTime creado;

	@Column(name="update_at" ,insertable = false, updatable = false)
	@Convert(converter = LocalDateTimeConverter.class)
	private LocalDateTime actualizado;


	

	public Credito(Cliente cliente, double valorPrestamo, double interes, boolean pagado, LocalDateTime fechaCredito) {
		super();
		this.cliente = cliente;
		this.valorPrestamo = valorPrestamo;
		this.interes = interes;
		this.pagado = pagado;
		this.fechaCredito = fechaCredito;
		calcularTotal();
		calcularDias();
	}

	public Credito() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Long getId() {
		return id;
	}

	public void calcularPagoDirario() {
		this.cobroDia = Utils.redondearDecimales(this.total / this.dias,3);
	}
	
	public void calcularTotal() {
		double intereses = this.valorPrestamo * this.interes /100; 
		this.total = this.valorPrestamo + intereses;
	}
	
	public void calcularDias() {
		java.util.Date out = (java.util.Date) Date.from(this.fechaCredito.atZone(ZoneId.systemDefault()).toInstant());
		if (out!=null) {
			this.fechaFinCredito = (java.util.Date) Utils.sumarFechasMeses(out,1);
			int cantidadDias = Utils.diferenciasDeFechas(out, this.fechaFinCredito);
			this.dias = cantidadDias;
		}else {
			this.dias = 1;
		}
		
	}
	
	public void setId(Long id) {
		this.id = id;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public double getValorPrestamo() {
		return valorPrestamo;
	}

	public void setValorPrestamo(double valorPrestamo) {
		this.valorPrestamo = valorPrestamo;
	}

	public double getInteres() {
		return interes;
	}

	public void setInteres(double interes) {
		this.interes = interes;
	}

	public boolean isPagado() {
		return pagado;
	}

	public void setPagado(boolean pagado) {
		this.pagado = pagado;
	}

	public LocalDateTime getFechaCredito() {
		return fechaCredito;
	}

	public void setFechaCredito(LocalDateTime fechaCredito) {
		this.fechaCredito = fechaCredito;
	}

	public LocalDateTime getCreado() {
		return creado;
	}

	public void setCreado(LocalDateTime creado) {
		this.creado = creado;
	}

	public LocalDateTime getActualizado() {
		return actualizado;
	}

	public void setActualizado(LocalDateTime actualizado) {
		this.actualizado = actualizado;
	}

	public double getTotal() {
		return total;
	}

	public void setTotal(double total) {
		this.total = total;
	}

	public int getDias() {
		return dias;
	}

	public void setDias(int dias) {
		this.dias = dias;
	}

	public double getCobroDia() {
		return cobroDia;
	}

	public void setCobroDia(double cobroDia) {
		this.cobroDia = cobroDia;
	}

	public java.util.Date getFechaFinCredito() {
		return fechaFinCredito;
	}

	public void setFechaFinCredito(java.util.Date fechaFinCredito) {
		this.fechaFinCredito = fechaFinCredito;
	}	
	
	
	
}

