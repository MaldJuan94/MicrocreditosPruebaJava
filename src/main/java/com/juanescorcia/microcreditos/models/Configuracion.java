package com.juanescorcia.microcreditos.models;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.data.jpa.convert.threeten.Jsr310JpaConverters.LocalDateTimeConverter;

@Entity
@Table(name="mc_configuracion")
public class Configuracion implements Serializable{

	@Id
	@Column(name = "pk")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "interes")
	private double interes;
	
	@Column(name = "cupomaximo")
	private double cupoMaximo;
	
	
	@Column(name="created_at",insertable = false, updatable = false)
	@Convert(converter = LocalDateTimeConverter.class)
	private LocalDateTime creado;

	@Column(name="update_at" ,insertable = false, updatable = false)
	@Convert(converter = LocalDateTimeConverter.class)
	private LocalDateTime actualizado;

	
	public Configuracion(double interes, double cupoMaximo) {
		super();
		this.interes = interes;
		this.cupoMaximo = cupoMaximo;
	}

	public Configuracion() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public double getInteres() {
		return interes;
	}

	public void setInteres(double interes) {
		this.interes = interes;
	}

	public LocalDateTime getCreado() {
		return creado;
	}

	public void setCreado(LocalDateTime creado) {
		this.creado = creado;
	}

	public LocalDateTime getActualizado() {
		return actualizado;
	}

	public void setActualizado(LocalDateTime actualizado) {
		this.actualizado = actualizado;
	}

	public double getCupoMaximo() {
		return cupoMaximo;
	}

	public void setCupoMaximo(double cupoaximo) {
		this.cupoMaximo = cupoaximo;
	}


	
}
