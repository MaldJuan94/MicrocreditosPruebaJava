package com.juanescorcia.microcreditos.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.juanescorcia.microcreditos.dao.ClienteDao;
import com.juanescorcia.microcreditos.models.Cliente;

@Service("clienteService")
@Transactional
public class ClienteServiceImpl implements ClienteService{

	@Autowired
	private ClienteDao _clienteDao;
	
	@Override
	public void saveCliente(Cliente cliente) {
		// TODO Auto-generated method stub
		_clienteDao.saveCliente(cliente);
	}

	@Override
	public void deleteClienteById(Long idCliente) {
		// TODO Auto-generated method stub
		_clienteDao.deleteClienteById(idCliente);
	}

	@Override
	public void updateCliente(Cliente cliente) {
		// TODO Auto-generated method stub
		_clienteDao.updateCliente(cliente);
	}

	@Override
	public List<Cliente> findAllClientes() {
		// TODO Auto-generated method stub
		return _clienteDao.findAllClientes();
	}

	@Override
	public Cliente findById(Long idCliente) {
		// TODO Auto-generated method stub
		return _clienteDao.findById(idCliente);
	}

	@Override
	public Cliente findByCC(String cc) {
		// TODO Auto-generated method stub
		return _clienteDao.findByCC(cc);
	}

}
