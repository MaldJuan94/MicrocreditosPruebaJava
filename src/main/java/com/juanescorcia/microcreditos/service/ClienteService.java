package com.juanescorcia.microcreditos.service;

import java.util.List;

import com.juanescorcia.microcreditos.models.Cliente;

public interface ClienteService {
	void saveCliente(Cliente cliente);
	
	void deleteClienteById(Long idCliente);
	
	void updateCliente(Cliente cliente);
	
	List<Cliente> findAllClientes();
	
	Cliente findById(Long idCliente);
	
	Cliente findByCC(String cc);
}
