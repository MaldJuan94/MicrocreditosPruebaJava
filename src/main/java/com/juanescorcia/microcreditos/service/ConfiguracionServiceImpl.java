package com.juanescorcia.microcreditos.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.juanescorcia.microcreditos.dao.ConfiguracionDao;
import com.juanescorcia.microcreditos.models.Configuracion;

@Service("configuracionService")
@Transactional
public class ConfiguracionServiceImpl implements ConfiguracionService {

	@Autowired
	private ConfiguracionDao _configuracionDao;
	
	@Override
	public Configuracion findLast() {
		// TODO Auto-generated method stub
		return _configuracionDao.findLast();
	}

}
