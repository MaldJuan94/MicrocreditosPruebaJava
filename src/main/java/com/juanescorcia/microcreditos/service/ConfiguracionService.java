package com.juanescorcia.microcreditos.service;

import com.juanescorcia.microcreditos.models.Configuracion;

public interface ConfiguracionService {
	Configuracion findLast();
}
