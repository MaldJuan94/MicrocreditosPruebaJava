package com.juanescorcia.microcreditos.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.juanescorcia.microcreditos.dao.CreditoDao;
import com.juanescorcia.microcreditos.models.Credito;

@Service("creditoService")
@Transactional
public class CreditoServiceImpl implements CreditoService{
	
	@Autowired
	private CreditoDao _creditoDao;
	
	@Override
	public void saveCredito(Credito credito) {
		// TODO Auto-generated method stub
		_creditoDao.saveCredito(credito);
	}

	@Override
	public void deleteCreditoById(Long idCredito) {
		// TODO Auto-generated method stub
		_creditoDao.deleteCreditoById(idCredito);
	}

	@Override
	public void updateCredito(Credito credito) {
		// TODO Auto-generated method stub
		_creditoDao.updateCredito(credito);
	}

	@Override
	public List<Credito> findAllCreditos() {
		// TODO Auto-generated method stub
		return _creditoDao.findAllCreditos();
	}

	@Override
	public List<Credito> findAllClientesByCliente(Long idCredito) {
		// TODO Auto-generated method stub
		return _creditoDao.findAllClientesByCliente(idCredito);
	}

	@Override
	public Credito findById(Long idCredito) {
		// TODO Auto-generated method stub
		return _creditoDao.findById(idCredito);
	}

	@Override
	public List<Credito> findAllByCliente(Long idCliente) {
		// TODO Auto-generated method stub
		return _creditoDao.findAllByCliente(idCliente);
	}

}
