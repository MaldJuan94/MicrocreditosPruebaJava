package com.juanescorcia.microcreditos.service;

import java.util.List;

import com.juanescorcia.microcreditos.models.Credito;

public interface CreditoService {
	void saveCredito(Credito credito);
	
	void deleteCreditoById(Long idCredito);
	
	void updateCredito(Credito credito);
	
	List<Credito> findAllCreditos();
	
	List<Credito> findAllClientesByCliente(Long idCredito);
	
	List<Credito> findAllByCliente(Long idCliente);
	
	Credito findById(Long idCredito);
}
