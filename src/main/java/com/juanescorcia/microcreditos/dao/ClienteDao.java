package com.juanescorcia.microcreditos.dao;

import java.util.List;

import com.juanescorcia.microcreditos.models.Cliente;

public interface ClienteDao {
	void saveCliente(Cliente cliente);
	
	void deleteClienteById(Long idCliente);
	
	void updateCliente(Cliente cliente);
	
	List<Cliente> findAllClientes();
	
	Cliente findById(Long idCliente);
	
	Cliente findByCC(String cc);
}
