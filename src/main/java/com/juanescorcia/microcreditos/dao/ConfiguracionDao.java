package com.juanescorcia.microcreditos.dao;

import com.juanescorcia.microcreditos.models.Configuracion;

public interface ConfiguracionDao {
	Configuracion findLast();
}
