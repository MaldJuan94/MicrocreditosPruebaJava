package com.juanescorcia.microcreditos.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.juanescorcia.microcreditos.models.Credito;
import com.juanescorcia.microcreditos.service.ClienteService;

@Repository
@Transactional
public class CreditoDaoImpl extends AbstractSession implements CreditoDao {
	@Autowired
	private ClienteService _clienteService;		
	
	@Override
	public void saveCredito(Credito credito) {
		// TODO Auto-generated method stub
		getSession().persist(credito);
	}

	@Override
	public void deleteCreditoById(Long idCredito) {
		// TODO Auto-generated method stub
		Credito credito = findById(idCredito);
		if (credito != null) {
			getSession().delete(credito);
		}
	}

	@Override
	public void updateCredito(Credito credito) {
		// TODO Auto-generated method stub
		getSession().update(credito);
	}

	@Override
	public List<Credito> findAllCreditos() {
		// TODO Auto-generated method stub
		return getSession().createQuery("from Credito").list();
	}

	@Override
	public List<Credito> findAllClientesByCliente(Long idCredito) {
		// TODO Auto-generated method stub
		return getSession().createQuery("from Credito where documento= :idcliente").setParameter("idcliente", idCredito).list();
	}

	@Override
	public Credito findById(Long idCredito) {
		// TODO Auto-generated method stub
		return (Credito) getSession().get(Credito.class, idCredito);
	}

	@Override
	public List<Credito> findAllByCliente(Long idCliente) {
		// TODO Auto-generated method stub
		return getSession().createQuery("from Credito where cliente.id= :idcliente and pagado= 0").setParameter("idcliente", idCliente).list();
	}

}
