package com.juanescorcia.microcreditos.dao;

import javax.transaction.Transactional;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import com.juanescorcia.microcreditos.models.Configuracion;

@Repository
@Transactional
public class ConfiguracionImpl  extends AbstractSession implements ConfiguracionDao {

	@Override
	public Configuracion findLast() {
		// TODO Auto-generated method stub
		Query query = getSession().createQuery("from Configuracion order by creado DESC");
		query.setMaxResults(1);
		Configuracion last = (Configuracion) query.uniqueResult();		
		return last;
	}

}
