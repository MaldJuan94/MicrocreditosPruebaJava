package com.juanescorcia.microcreditos.dao;

import java.util.List;


import com.juanescorcia.microcreditos.models.Credito;

public interface CreditoDao {
	void saveCredito(Credito credito);
	
	void deleteCreditoById(Long idCredito);
	
	void updateCredito(Credito credito);
	
	List<Credito> findAllCreditos();
	
	List<Credito> findAllClientesByCliente(Long idCredito);
	
	Credito findById(Long idCredito);
	
	List<Credito> findAllByCliente(Long idCliente);
	
}
