package com.juanescorcia.microcreditos.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import com.juanescorcia.microcreditos.models.Cliente;

@Repository
@Transactional
public class ClienteDaoImpl extends AbstractSession implements ClienteDao {

	@Override
	public void saveCliente(Cliente cliente) {
		// TODO Auto-generated method stub
		getSession().persist(cliente);
	}

	@Override
	public void deleteClienteById(Long idCliente) {
		// TODO Auto-generated method stub
		Cliente cliente = findById(idCliente);
		if (cliente != null) {
			getSession().delete(cliente);
		}
	}

	@Override
	public void updateCliente(Cliente cliente) {
		// TODO Auto-generated method stub
		getSession().update(cliente);
		
	}

	@Override
	public List<Cliente> findAllClientes() {
		// TODO Auto-generated method stub
		return getSession().createQuery("from Cliente").list();
	}

	@Override
	public Cliente findById(Long idCliente) {
		// TODO Auto-generated method stub
		return (Cliente) getSession().get(Cliente.class, idCliente);
	}

	@Override
	public Cliente findByCC(String cc) {
		// TODO Auto-generated method stub
		return (Cliente) getSession().createQuery(
				"from Cliente where documento = :cc")
				.setParameter("cc", cc).uniqueResult();
	}

}
