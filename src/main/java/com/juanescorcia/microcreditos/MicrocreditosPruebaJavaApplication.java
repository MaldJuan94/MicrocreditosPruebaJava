package com.juanescorcia.microcreditos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MicrocreditosPruebaJavaApplication {

	public static void main(String[] args) {
		SpringApplication.run(MicrocreditosPruebaJavaApplication.class, args);
	}
}
