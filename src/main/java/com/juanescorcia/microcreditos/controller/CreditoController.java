package com.juanescorcia.microcreditos.controller;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import com.juanescorcia.microcreditos.models.Cliente;
import com.juanescorcia.microcreditos.models.Configuracion;
import com.juanescorcia.microcreditos.models.Credito;
import com.juanescorcia.microcreditos.service.ClienteService;
import com.juanescorcia.microcreditos.service.ConfiguracionService;
import com.juanescorcia.microcreditos.service.CreditoService;
import com.juanescorcia.microcreditos.util.CustomErrorType;
import com.juanescorcia.microcreditos.util.Utils;

@Controller
@RequestMapping("/v1")
public class CreditoController {

	@Autowired
	private CreditoService _creditoService;	
	
	@Autowired
	private ConfiguracionService _configuracionService;	
	
	@Autowired
	private ClienteService _clienteService;		
	
	//GET
	@CrossOrigin(origins = "*")
	@RequestMapping(value="/Creditos", method = RequestMethod.GET, headers = "Accept=application/json")
	public ResponseEntity<List<Credito>> getCreditos()  {
		List<Credito> creditos = new ArrayList<>();
			creditos = _creditoService.findAllCreditos();
			if (creditos.isEmpty()) {
				return new ResponseEntity(HttpStatus.NO_CONTENT);
			}
			for (Credito credito : creditos) {
				credito.calcularDias();
				credito.calcularTotal();
				credito.calcularPagoDirario();
			}
			
			return new ResponseEntity<List<Credito>>(creditos,HttpStatus.OK);
	}	
	
	
	//GET
	@CrossOrigin(origins = "*")
	@RequestMapping(value="/Creditos/Proyeccion", method = RequestMethod.GET, headers = "Accept=application/json")
	public ResponseEntity<?> getProyeccion(@RequestParam(value="valor", required = true) double valor ,@RequestParam(value="fecha", required = true) String fechaCredito)  {
			Configuracion config = _configuracionService.findLast();
			if (config==null) {
				return new ResponseEntity(new CustomErrorType("interes is requiered"), HttpStatus.CONFLICT);
			}
			if (valor<=config.getCupoMaximo()) {
				
			if (valor<=0) {
				HashMap<String, String> result = new HashMap<String, String>();  
				result.put("error","El valor de préstamo debe ser mayor que cero ");  
				return new ResponseEntity<HashMap<String, String>>(result,HttpStatus.OK);		
			}
			DecimalFormat formatNumber = new DecimalFormat("#.00");
			SimpleDateFormat formatDate = new SimpleDateFormat("dd/MM/yy"); 

			if (fechaCredito==null) {
				return new ResponseEntity(new CustomErrorType("fechaCredito is requiered"), HttpStatus.CONFLICT);
			}
	
			Date FormatFecha = Utils.deStringToDate(fechaCredito);
			if (FormatFecha==null) {
				return new ResponseEntity(new CustomErrorType("fechaCredito is incorrect"), HttpStatus.CONFLICT);
			}
			Date FormatFechaFin = Utils.sumarFechasMeses(FormatFecha,1);
			int cantidadDias = Utils.diferenciasDeFechas(FormatFecha, FormatFechaFin);
			double intereses = valor * config.getInteres() /100; 
			
			
			HashMap<String, String> result = new HashMap<String, String>();  
			result.put("cantidadDias",cantidadDias+"");  
			result.put("porcentajeInteres", Utils.redondearDecimales(config.getInteres(),3)+"");  
			result.put("interes",Utils.redondearDecimales(intereses,3)+"");  
			result.put("cobrosDiarios",Utils.redondearDecimales((intereses+valor)/cantidadDias,3)+"");  
			result.put("totalPagar",Utils.redondearDecimales((intereses+valor),3)+"");  
			result.put("fechaFin",formatDate.format(FormatFechaFin));  
			return new ResponseEntity<HashMap<String, String>>(result,HttpStatus.OK);
		}else {
			HashMap<String, String> result = new HashMap<String, String>();  
			result.put("error","Cupo de préstamo excedido ");  
			return new ResponseEntity<HashMap<String, String>>(result,HttpStatus.OK);
		}
	}
	
	//GET
	@CrossOrigin(origins = "*")
	@RequestMapping(value="/Creditos/Creaar", method = RequestMethod.GET, headers = "Accept=application/json")
	public ResponseEntity<?> createCliente(@RequestParam(value="valor", required = true) double valorPrestamo ,@RequestParam(value="fecha", required = true) String fechaCredito,@RequestParam(value="cliente", required = true) Long ClienteId){
		HashMap<String, String> error = new HashMap<String, String>();
		Configuracion config = _configuracionService.findLast();
		
		if (config==null) {
			error.put("error","No se encontró valor para el interés.");  
			return new ResponseEntity<HashMap<String, String>>(error,HttpStatus.OK);
		}	

		if (valorPrestamo<=config.getCupoMaximo()) {	
			if (valorPrestamo<=0) {
				HashMap<String, String> result = new HashMap<String, String>();  
				result.put("error","El valor de préstamo debe ser mayor que cero ");  
				return new ResponseEntity<HashMap<String, String>>(result,HttpStatus.OK);		
			}
		
			if (ClienteId==null || fechaCredito==null) {
				error.put("error","Faltan parametros");  
				return new ResponseEntity<HashMap<String, String>>(error,HttpStatus.OK);
			}
			Cliente cliente = _clienteService.findById(ClienteId);
			
			if (cliente==null) {
				error.put("error","Cliente invalido");  
				return new ResponseEntity<HashMap<String, String>>(error,HttpStatus.OK);
			}
			List<Credito> creditosActivos =_creditoService.findAllByCliente(cliente.getId());
			
			System.err.println(creditosActivos.size());
			if (!creditosActivos.isEmpty()) {
				error.put("error","Hay un crédito vigente,cancele el crédito.");  
				return new ResponseEntity<HashMap<String, String>>(error,HttpStatus.OK);			
			}
	
			Date FormatFecha = Utils.deStringToDate(fechaCredito);
			if (FormatFecha==null) {
				error.put("error","Fecha crédito invalida.");  
				return new ResponseEntity<HashMap<String, String>>(error,HttpStatus.OK);
			}
			
			ZoneId defaultZoneId = ZoneId.systemDefault();
			Instant instant = FormatFecha.toInstant();
			LocalDateTime localDateTime = instant.atZone(defaultZoneId).toLocalDateTime();
			Credito currentCredito = new Credito();
			currentCredito.setCliente(cliente);
			currentCredito.setInteres(config.getInteres());
			currentCredito.setValorPrestamo(valorPrestamo);
			currentCredito.setFechaCredito(localDateTime);
			
			_creditoService.saveCredito(currentCredito);
			
			return new ResponseEntity<Credito>(currentCredito,HttpStatus.OK);
		
		}else {
			HashMap<String, String> result = new HashMap<String, String>();  
			result.put("error","Cupo de préstamo excedido ");  
			return new ResponseEntity<HashMap<String, String>>(result,HttpStatus.OK);
		}
	}
	
	
	
}
