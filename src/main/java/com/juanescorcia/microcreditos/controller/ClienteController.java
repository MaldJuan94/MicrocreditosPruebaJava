package com.juanescorcia.microcreditos.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.util.UriComponentsBuilder;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.juanescorcia.microcreditos.models.Cliente;
import com.juanescorcia.microcreditos.service.ClienteService;
import com.juanescorcia.microcreditos.util.CustomErrorType;

@Controller
@RequestMapping("/v1")
public class ClienteController {
	
	@Autowired
	private ClienteService _clienteService;	

	//GET
	@CrossOrigin(origins = "*")
	@RequestMapping(value="/Clientes", method = RequestMethod.GET, headers = "Accept=application/json")
	public ResponseEntity<List<Cliente>> getClientes(@RequestParam(value="documento", required = false) String documento)  {
		
		List<Cliente> clientes = new ArrayList<>();
		
		if (documento==null) {
			clientes = _clienteService.findAllClientes();
			if (clientes.isEmpty()) {
				return new ResponseEntity(HttpStatus.NO_CONTENT);
			}
			for (Cliente cliente : clientes) {
				cliente.FormatNombreCompleto();
			}
			return new ResponseEntity<List<Cliente>>(clientes,HttpStatus.OK);
		}else {
			Cliente cliente = _clienteService.findByCC(documento);
			if (cliente==null) {
				return new ResponseEntity(HttpStatus.NOT_FOUND);
			}
			cliente.FormatNombreCompleto();
			clientes.add(cliente);
			return new ResponseEntity<List<Cliente>>(clientes,HttpStatus.OK);
		}
	}
	//GET 
	@CrossOrigin(origins = "*")
	@RequestMapping(value="/Clientes/{id}", method = RequestMethod.GET, headers = "Accept=application/json")
	public ResponseEntity<Cliente> getClientesById(@PathVariable("id") Long idCliente)  {
		if (idCliente == null || idCliente <=0) {
			return new ResponseEntity(new CustomErrorType("idCliente is requiered"), HttpStatus.CONFLICT);
		}
		Cliente cliente = _clienteService.findById(idCliente);
		if (cliente ==null) {
			return new ResponseEntity(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<Cliente>(cliente,HttpStatus.OK);

	}
	
	
	//POST
	@CrossOrigin(origins = "*")
	@RequestMapping(value="/Clientes", method = RequestMethod.POST, headers = "Accept=application/json")
	public ResponseEntity<?> createCliente(@RequestBody Cliente cliente, UriComponentsBuilder uriComponentsBuilder){
		if (cliente.getDocumento().equals(null) || cliente.getDocumento().isEmpty()) {
			return new ResponseEntity(new CustomErrorType("documento is requiered"), HttpStatus.CONFLICT);
		}
		
		if (_clienteService.findByCC(cliente.getDocumento()) != null) {
			return new ResponseEntity(new CustomErrorType("documento is unique"), HttpStatus.CONFLICT);
		}
		
		_clienteService.saveCliente(cliente);
		Cliente clienteNew = _clienteService.findByCC(cliente.getDocumento());
		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(
				uriComponentsBuilder.path("/v1/Clientes/{id}")
				.buildAndExpand(clienteNew.getId())
				.toUri()
				);
		
		return new ResponseEntity<String>(headers, HttpStatus.CREATED);
	}
	
	//UPDATE
	@CrossOrigin(origins = "*")
	@RequestMapping(value="/Clientes/{id}", method = RequestMethod.PATCH, headers = "Accept=application/json")
	public ResponseEntity<?> updateCliente (@PathVariable("id") Long idCliente , @RequestBody Cliente cliente)  {
		if (idCliente == null || idCliente <=0) {
			return new ResponseEntity(HttpStatus.NO_CONTENT);
		}
		Cliente currentCliente = _clienteService.findById(idCliente);
		if (currentCliente==null) {
			return new ResponseEntity(HttpStatus.NO_CONTENT);
		}
		if (!cliente.getPrimerNombre().equals("") && cliente.getPrimerNombre()!=null)
			currentCliente.setPrimerNombre(cliente.getPrimerNombre());
		if (!cliente.getSegundoNombre().equals("") && cliente.getSegundoNombre()!=null)
			currentCliente.setSegundoNombre(cliente.getSegundoNombre());
		if (!cliente.getPrimerApellido().equals("") && cliente.getPrimerApellido()!=null)
			currentCliente.setPrimerApellido(cliente.getPrimerApellido());
		if (!cliente.getSegundoApellido().equals("") && cliente.getSegundoApellido()!=null)
			currentCliente.setSegundoApellido(cliente.getSegundoApellido());
		if (!cliente.getTelefono().equals("") && cliente.getTelefono()!=null)
			currentCliente.setTelefono(cliente.getTelefono());
		if (!cliente.getDireccion().equals("") && cliente.getDireccion()!=null)
			currentCliente.setDireccion(cliente.getDireccion());
		
		if (!cliente.getDocumento().equals("") && cliente.getDocumento()!=null) {
			Cliente ClientevalCC = _clienteService.findByCC(cliente.getDocumento());
			if (ClientevalCC!=null) {
				if (ClientevalCC.getId()!=currentCliente.getId()) {
					return new ResponseEntity(new CustomErrorType("documento is unique"), HttpStatus.CONFLICT);
				}
			}else {
				currentCliente.setDocumento(cliente.getDocumento());
			}
		}
			

		_clienteService.updateCliente(currentCliente);
		return new ResponseEntity<Cliente>(currentCliente,HttpStatus.OK);
	}
	
	//DELETE
	@CrossOrigin(origins = "*")
	@RequestMapping(value="/Clientes/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<?> deleteCliente(@PathVariable("id") Long idCliente)  {
		if (idCliente == null || idCliente <=0) {
			return new ResponseEntity(new CustomErrorType("idCliente is requiered"), HttpStatus.CONFLICT);
		}
		Cliente cliente = _clienteService.findById(idCliente);
		if (cliente ==null) {
			return new ResponseEntity(HttpStatus.NO_CONTENT);
		}
		
		_clienteService.deleteClienteById(idCliente);
		
		return new ResponseEntity<Cliente>(HttpStatus.OK);

	}
}
